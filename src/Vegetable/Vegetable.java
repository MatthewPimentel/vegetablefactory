/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vegetable;

/**
 *
 * @author Matt
 */
abstract class Vegetable {
    
    private String color;
    private double size;
  

    Vegetable(String color, double size) {
        this.color = color;
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }
    
    abstract String isRipe();

    @Override
    public String toString() {
        return  "color=" + color + ", size=" + size;
    }
}

